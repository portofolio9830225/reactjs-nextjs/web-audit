import React, { lazy } from 'react';

// define manual import for preventing hook restriction
const DUMMY = {
	CRUD: lazy(() => import('../views/ExampleCRUD')),
};

const MASTER_DATA = {
	MasterTeam: lazy(() => import('../views/masterData/Team')),
};

const contents = [
	// DUMMY
	{
		path: '/page-testing/crud',
		element: <DUMMY.CRUD />,
		index: 'CRUD',
		exact: true,
	},
	{
		path: '/master-data/team',
		element: <MASTER_DATA.MasterTeam />,
		index: 'MasterTeam',
		exact: true,
	},
];
export default contents;

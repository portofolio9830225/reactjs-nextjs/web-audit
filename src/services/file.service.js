import axios from 'axios';
import authHeader from './auth-header';

const API_URL_DEFAULT = process.env.REACT_APP_API;

const create = async (payload) => {
	return axios.post(`${API_URL_DEFAULT}file`, payload, {
		headers: await authHeader(),
	});
};

const readById = async (query) => {
	return axios.get(`${API_URL_DEFAULT}file/by-onedrive-id`, {
		headers: await authHeader(),
		params: query,
	});
};

export default { create, readById };

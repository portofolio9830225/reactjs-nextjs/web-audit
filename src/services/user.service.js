import axios from 'axios';
import authHeader from './auth-header';

const API_URL_DEFAULT = process.env.REACT_APP_API;

const getUserBy = async (query) => {
	return axios.get(`${API_URL_DEFAULT}user/by-dept-div-pos`, {
		headers: await authHeader(),
		params: query,
	});
};

const getAllDepartments = async () => {
	return axios.get(`${API_URL_DEFAULT}user/all-department`, {
		headers: await authHeader(),
	});
};

const getTeam = async (query) => {
	return axios.get(`${API_URL_DEFAULT}user/store-team`, {
		headers: await authHeader(),
		params: query,
	});
};

const storeTeam = async (payload) => {
	return axios.post(`${API_URL_DEFAULT}user/store-team`, payload, {
		headers: await authHeader(),
	});
};

const updateTeam = async (payload) => {
	return axios.put(`${API_URL_DEFAULT}user/update-team`, payload, {
		headers: await authHeader(),
	});
};

const deleteTeam = async (query) => {
	return axios.delete(`${API_URL_DEFAULT}user/delete-team`, {
		headers: await authHeader(),
		params: query,
	});
};

export default { getUserBy, getAllDepartments, storeTeam, getTeam, deleteTeam, updateTeam };

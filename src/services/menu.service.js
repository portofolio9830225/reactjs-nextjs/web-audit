import axios from 'axios';
import authHeader from './auth-header';

const API_URL_DEFAULT = process.env.REACT_APP_API;

const createMenu = async (payload) => {
	return axios.post(`${API_URL_DEFAULT}menu`, payload, { headers: await authHeader() });
};

const readMenu = async (query) => {
	return axios.get(`${API_URL_DEFAULT}menu?${query}`, { headers: await authHeader() });
};

const updateMenu = async (payload) => {
	return axios.put(`${API_URL_DEFAULT}menu`, payload, { headers: await authHeader() });
};

const deleteMenu = async (payload) => {
	return axios.delete(`${API_URL_DEFAULT}menu`, { data: payload, headers: await authHeader() });
};

const createSubMenu = async (payload) => {
	return axios.post(`${API_URL_DEFAULT}submenu`, payload, { headers: await authHeader() });
};

const readSubMenu = async (query) => {
	return axios.get(`${API_URL_DEFAULT}submenu?${query}`, { headers: await authHeader() });
};

const updateSubMenu = async (payload) => {
	return axios.put(`${API_URL_DEFAULT}submenu`, payload, { headers: await authHeader() });
};

const deleteSubMenu = async (payload) => {
	return axios.delete(`${API_URL_DEFAULT}submenu`, {
		data: payload,
		headers: await authHeader(),
	});
};

export default {
	createMenu,
	readMenu,
	updateMenu,
	deleteMenu,
	createSubMenu,
	readSubMenu,
	updateSubMenu,
	deleteSubMenu,
};

import React, { useState, useEffect } from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import moment from 'moment';
import Swal from 'sweetalert2';
import DatePicker from 'react-datepicker';

import useDarkMode from '../../hooks/useDarkMode';
import UserModule from '../../modules/UserModule';

import PageWrapper from '../../layout/PageWrapper/PageWrapper';
import PageLayoutHeader from '../../pages/common/Headers/PageLayoutHeader';
import Page from '../../layout/Page/Page';
import Card, { CardBody, CardHeader, CardLabel, CardTitle } from '../../components/bootstrap/Card';
import Modal, { ModalBody, ModalHeader, ModalTitle } from '../../components/bootstrap/Modal';
import Button from '../../components/bootstrap/Button';
import FormGroup from '../../components/bootstrap/forms/FormGroup';
import Input from '../../components/bootstrap/forms/Input';
import CustomSelect from '../../components/CustomSelect';
import Icon from '../../components/icon/Icon';
import Label from '../../components/bootstrap/forms/Label';
import DarkDataTable from '../../components/DarkDataTable';
import showNotification from '../../components/extras/showNotification';

import 'react-datepicker/dist/react-datepicker.css';

const FormEdit = (dt) => {
	const { setIsEdit, listMember, row, fetchData } = dt;
	const { darkModeStatus } = useDarkMode();

	const [dateYear, setDateYear] = useState(new Date(`01-01-${row.periode}`));
	const [newMember, setNewMember] = useState(null);
	const [dataM, setDataM] = useState(row.member);

	const SelectMember = (e) => {
		const m = e.value.split('|');

		if (dataM.length > 0 && dataM.length < 3) {
			const new_data = [...dataM];

			const filter = new_data.filter((el) => el.username == m[5]);

			if (filter.length == 0) {
				new_data.push({
					username: m[5],
					email: m[2],
					name: m[3],
					department: m[0],
					division: m[1],
					position: m[4],
				});
				setDataM(new_data);
			}
		} else {
			const data = [];
			data.push({
				username: m[5],
				email: m[2],
				name: m[3],
				department: m[0],
				division: m[1],
				position: m[4],
			});
			setDataM(data);
		}

		setNewMember(e);
	};

	const deleteMember = (name) => {
		const new_data = dataM.filter((e) => e.name != name);
		setDataM(new_data);
	};

	const formik = useFormik({
		initialValues: {
			team: row.team_name,
			periode: row.periode,
		},
		validationSchema: Yup.object({
			team: Yup.string().required('Required'),
		}),
		onSubmit: async (val) => {
			val.members = dataM;
			val.id = row._id;
			val.periode = moment(dateYear).format('YYYY');

			UserModule.updateTeam(val)
				.then((res) => {
					if (res == 'sukses') {
						fetchData(1, 10);
						setIsEdit(false);
						showNotification('Success!', 'Success Update', 'success');
					} else {
						showNotification('Warning!', 'Failed Update', 'danger');
					}
				})
				.catch(() => {
					showNotification('Warning!', 'Failed Update', 'danger');
				});
		},
	});

	return (
		<form onSubmit={formik.handleSubmit}>
			<FormGroup id='team' label='Team Name' className='col-md-12 mb-3'>
				<Input
					onChange={formik.handleChange}
					onBlur={formik.handleBlur}
					value={formik.values.team}
					isValid={formik.isValid}
					isTouched={formik.touched.team}
					invalidFeedback={formik.errors.team}
					autoComplete='off'
				/>
			</FormGroup>
			<FormGroup id='periode' label='Periode' className='col-md-12 mb-3'>
				<DatePicker
					selected={dateYear}
					onChange={(date) => setDateYear(date)}
					showYearPicker
					dateFormat='yyyy'
					className='form-control'
				/>
			</FormGroup>
			<FormGroup id='add_members' label='Add Members' className='col-md-12 mb-3'>
				<CustomSelect
					isSearchable
					placeholder='Member'
					onChange={(e) => SelectMember(e)}
					value={newMember}
					options={listMember}
					darkTheme={darkModeStatus}
				/>
			</FormGroup>
			<Label className='md-3' htmlFor='members'>
				Member
			</Label>
			<div className='mb-5'>
				<div className='mb-3'>
					{dataM.length > 0 &&
						dataM.map((e) => (
							<div className='w-100 d-flex justify-content-between align-items-center mb-3'>
								<div>
									<p className='mb-0 fw-bold'>{e.name}</p>
									<span style={{ color: 'grey' }}>
										{e.username} - {e.division}
									</span>
								</div>
								<div>
									<Button onClick={() => deleteMember(e.name)}>
										<div
											style={{
												width: '25px',
												height: '25px',
												borderRadius: '50%',
											}}
											className='bg-danger d-flex justify-content-center align-items-center'>
											<Icon icon='Close' color='light' size='md' />
										</div>
									</Button>
								</div>
							</div>
						))}
				</div>
				<p className='mb-0' style={{ color: '#3A98B9' }}>
					{dataM.length} of 3 Maximum Member
				</p>
			</div>
			<div className='w-100 d-flex justify-content-end'>
				<div>
					<Button type='button' color='primary' isLink onClick={() => setIsEdit(false)}>
						Close
					</Button>
					<Button type='submit' className='btn btn-primary ms-2'>
						Submit
					</Button>
				</div>
			</div>
		</form>
	);
};

const FormTambah = (dt) => {
	const { setIsOpen, listMember, fetchData } = dt;
	const { darkModeStatus } = useDarkMode();

	const [dateYear, setDateYear] = useState(new Date());
	const [newMember, setNewMember] = useState(null);
	const [dataM, setDataM] = useState([]);

	const SelectMember = (e) => {
		const m = e.value.split('|');

		if (dataM.length > 0 && dataM.length < 3) {
			const new_data = [...dataM];

			const filter = new_data.filter((el) => el.username == m[5]);

			if (filter.length == 0) {
				new_data.push({
					username: m[5],
					email: m[2],
					name: m[3],
					department: m[0],
					division: m[1],
					position: m[4],
				});
				setDataM(new_data);
			}
		} else {
			const data = [];
			data.push({
				username: m[5],
				email: m[2],
				name: m[3],
				department: m[0],
				division: m[1],
				position: m[4],
			});
			setDataM(data);
		}

		setNewMember(e);
	};

	const deleteMember = (name) => {
		const new_data = dataM.filter((e) => e.name != name);
		setDataM(new_data);
	};

	const formik = useFormik({
		initialValues: {
			team: '',
			periode: '',
		},
		validationSchema: Yup.object({
			team: Yup.string().required('Required'),
		}),
		onSubmit: async (val) => {
			val.members = dataM;
			val.periode = moment(dateYear).format('YYYY');

			UserModule.storeTeam(val)
				.then((res) => {
					if (res == 'sukses') {
						fetchData(1, 10);
						setIsOpen(false);
						showNotification('Success!', 'Success Create', 'success');
					} else {
						showNotification('Warning!', 'Failed Create', 'danger');
					}
				})
				.catch(() => {
					showNotification('Warning!', 'Failed Create', 'danger');
				});
		},
	});

	return (
		<form onSubmit={formik.handleSubmit}>
			<FormGroup id='team' label='Team Name' className='col-md-12 mb-3'>
				<Input
					onChange={formik.handleChange}
					onBlur={formik.handleBlur}
					value={formik.values.team}
					isValid={formik.isValid}
					isTouched={formik.touched.team}
					invalidFeedback={formik.errors.team}
					autoComplete='off'
				/>
			</FormGroup>
			<FormGroup id='periode' label='Periode' className='col-md-12 mb-3'>
				<DatePicker
					selected={dateYear}
					onChange={(date) => setDateYear(date)}
					showYearPicker
					dateFormat='yyyy'
					className='form-control'
				/>
			</FormGroup>
			<FormGroup id='add_members' label='Add Members' className='col-md-12 mb-3'>
				<CustomSelect
					isSearchable
					placeholder='Member'
					onChange={(e) => SelectMember(e)}
					value={newMember}
					options={listMember}
					darkTheme={darkModeStatus}
				/>
			</FormGroup>
			<Label className='md-3' htmlFor='members'>
				Member
			</Label>
			<div className='mb-5'>
				<div className='mb-3'>
					{dataM.length > 0 &&
						dataM.map((e) => (
							<div className='w-100 d-flex justify-content-between align-items-center mb-3'>
								<div>
									<p className='mb-0 fw-bold'>{e.name}</p>
									<span style={{ color: 'grey' }}>
										{e.username} - {e.division}
									</span>
								</div>
								<div>
									<Button onClick={() => deleteMember(e.name)}>
										<div
											style={{
												width: '25px',
												height: '25px',
												borderRadius: '50%',
											}}
											className='bg-danger d-flex justify-content-center align-items-center'>
											<Icon icon='Close' color='light' size='md' />
										</div>
									</Button>
								</div>
							</div>
						))}
				</div>
				<p className='mb-0' style={{ color: '#3A98B9' }}>
					{dataM.length} of 3 Maximum Member
				</p>
			</div>
			<div className='w-100 d-flex justify-content-end'>
				<div>
					<Button type='button' color='primary' isLink onClick={() => setIsOpen(false)}>
						Close
					</Button>
					<Button type='submit' className='btn btn-primary ms-2'>
						Submit
					</Button>
				</div>
			</div>
		</form>
	);
};

const ButtonDetail = (dt) => {
	const { row, fetchData, setIsEdit, setRowEdit } = dt;
	const { darkModeStatus } = useDarkMode();

	const deleteTeam = () => {
		Swal.fire({
			title: 'Are you sure?',
			text: 'Please check your entries !',
			icon: 'question',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes',
		}).then((result) => {
			if (result.isConfirmed) {
				UserModule.deleteTeam({
					id: row._id,
				}).then((res) => {
					if (res == 'sukses') {
						fetchData(1, 10);
						showNotification('Success!', 'Success Delete', 'success');
					} else {
						showNotification('Warning!', 'Failed Delete', 'danger');
					}
				});
			} else if (result.dismiss === Swal.DismissReason.cancel) {
				Swal.fire('Cancelled', 'Your data is safe :)', 'error');
			}
		});
	};

	return (
		<div>
			<Button
				color='info'
				type='button'
				isLight={darkModeStatus}
				className='m-1'
				onClick={() => {
					setRowEdit(row);
					setIsEdit(true);
				}}>
				<Icon icon='Edit' size='md' />
			</Button>
			<Button
				color='danger'
				type='button'
				isLight={darkModeStatus}
				className='m-1'
				onClick={() => deleteTeam()}>
				<Icon icon='RestoreFromTrash' size='md' />
			</Button>
		</div>
	);
};

const CustomDataTable = (dt) => {
	const {
		data,
		totalRows,
		totalperPage,
		handlePageChange,
		loading,
		handlePerRowsChange,
		fetchData,
		setIsEdit,
		setRowEdit,
	} = dt;
	const { darkModeStatus } = useDarkMode();

	const columns = [
		{
			name: 'Team Name',
			selector: (row) => row.team_name,
			sortable: true,
		},
		{
			name: 'Team Member',
			// eslint-disable-next-line react/no-unstable-nested-components
			cell: (el) => {
				const total = el.member.length;

				return el.member.map((e, i) => {
					if (i == total - 1) {
						return `${e.name}`;
					}

					return `${e.name}, `;
				});
			},
		},
		{
			name: 'Periode',
			selector: (row) => row.periode,
			sortable: true,
		},
		{
			name: 'Created at',
			// eslint-disable-next-line react/no-unstable-nested-components
			cell: (row) => {
				return moment(row.created_at).format('DD MMM YYYY HH:mm');
			},
		},
		{
			name: 'Action',
			// eslint-disable-next-line react/no-unstable-nested-components
			cell: (el) => {
				return (
					<ButtonDetail
						row={el}
						fetchData={fetchData}
						setIsEdit={setIsEdit}
						setRowEdit={setRowEdit}
					/>
				);
			},
		},
	];

	return (
		<DarkDataTable
			columns={columns}
			data={data}
			pagination
			paginationServer
			paginationTotalRows={totalRows}
			paginationPerPage={totalperPage}
			theme={darkModeStatus ? 'custom_dark' : 'light'}
			onChangePage={handlePageChange}
			progressPending={loading}
			onChangeRowsPerPage={handlePerRowsChange}
		/>
	);
};

const Team = () => {
	const [isOpen, setIsOpen] = useState(false);
	const [isEdit, setIsEdit] = useState(false);
	const [listMember, setListMember] = useState([]);

	const [rowEdit, setRowEdit] = useState({});

	const [loading, setLoading] = useState(false);
	const [data, setData] = useState([]);
	const [pages, setPages] = useState(1);
	const [perPages, setPerPages] = useState(10);
	const [total, setTotal] = useState(0);

	const fetchMember = async () => {
		return UserModule.getUserBy({
			dept: 'MIS',
		}).then((res) => {
			const m = res.map((e) => {
				return {
					value: `${e.department}|${e.division}|${e.email}|${e.name}|${e.position}|${e.username}`,
					text: `${e.name}`,
					label: `${e.name}`,
				};
			});

			setListMember(m);
		});
	};

	const fetchData = async (page, perPage) => {
		setLoading(true);
		return UserModule.getTeam(page, perPage).then((results) => {
			setData(results.data);
			setTotal(results.countData);
			setLoading(false);
		});
	};

	useEffect(() => {
		fetchMember();
	}, []);

	useEffect(() => {
		fetchData(pages, perPages);
	}, [pages, perPages]);

	return (
		<PageWrapper title='Audit'>
			<PageLayoutHeader />
			<Page container='fluid'>
				<Card stretch>
					<CardHeader borderSize={1}>
						<div className='d-flex justify-content-between align-items-center w-100'>
							<div>
								{' '}
								<CardLabel icon='People' iconColor='info'>
									<CardTitle className='col-12'>Auditor Team</CardTitle>
								</CardLabel>
							</div>
							<div>
								<Button
									className='btn btn-outline-primary'
									onClick={() => setIsOpen(true)}>
									Create New +
								</Button>
							</div>
						</div>
					</CardHeader>
					<CardBody>
						<CustomDataTable
							data={data}
							totalRows={total}
							totalperPage={perPages}
							loading={loading}
							handlePageChange={setPages}
							handlePerRowsChange={setPerPages}
							fetchData={fetchData}
							setIsEdit={setIsEdit}
							setRowEdit={setRowEdit}
						/>
					</CardBody>
				</Card>
			</Page>

			<Modal
				isCentered
				isStaticBackdrop
				isOpen={isOpen}
				setIsOpen={setIsOpen}
				size='md'
				titleId='modal-create-auditor'>
				<ModalHeader setIsOpen={setIsOpen} className='p-4'>
					<ModalTitle id='modal-create-auditor'>Create New Auditor Team</ModalTitle>
				</ModalHeader>
				<ModalBody>
					<FormTambah
						setIsOpen={setIsOpen}
						listMember={listMember}
						fetchData={fetchData}
					/>
				</ModalBody>
			</Modal>

			<Modal
				isCentered
				isStaticBackdrop
				isOpen={isEdit}
				setIsOpen={setIsEdit}
				size='md'
				titleId='modal-create-auditor'>
				<ModalHeader setIsOpen={setIsEdit} className='p-4'>
					<ModalTitle id='modal-create-auditor'>Edit Auditor Team</ModalTitle>
				</ModalHeader>
				<ModalBody>
					<FormEdit
						setIsEdit={setIsEdit}
						listMember={listMember}
						row={rowEdit}
						fetchData={fetchData}
					/>
				</ModalBody>
			</Modal>
		</PageWrapper>
	);
};

export default Team;

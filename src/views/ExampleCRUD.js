import React, { useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useFormik } from 'formik';
import Swal from 'sweetalert2';
import PropTypes from 'prop-types';
import Accordion, { AccordionItem } from '../components/bootstrap/Accordion';
import Card, { CardBody, CardHeader, CardLabel, CardTitle } from '../components/bootstrap/Card';
import Page from '../layout/Page/Page';
import PageWrapper from '../layout/PageWrapper/PageWrapper';
import MenuModule from '../modules/MenuModule';
import Notifications from '../components/Notifications';
import PageLayoutHeader from '../pages/common/Headers/PageLayoutHeader';
import FormGroup from '../components/bootstrap/forms/FormGroup';
import Input from '../components/bootstrap/forms/Input';
import Button from '../components/bootstrap/Button';
import useDarkMode from '../hooks/useDarkMode';
import DarkDataTable from '../components/DarkDataTable';
import Modal, { ModalBody, ModalHeader, ModalTitle } from '../components/bootstrap/Modal';
import { getRequester } from '../helpers/helpers';

const handleCreate = (values) => {
	const newResponse = new Promise((resolve, reject) => {
		try {
			MenuModule.createMenu(values)
				.then(() => {
					Notifications.showNotif({
						header: 'Information!',
						message: 'Data has been saved successfully',
						type: Notifications.TYPE.SUCCESS,
					});
					resolve({ error: false, message: 'successfully' });
				})
				.catch((err) => {
					Notifications.showNotif({
						header: 'Warning!!',
						message: err,
						type: Notifications.TYPE.ERROR,
					});
					reject(new Error(err));
				});
		} catch (e) {
			reject(new Error(e.message));
		}
	});
	return newResponse;
};
const handleUpdate = (values) => {
	const newResponse = new Promise((resolve, reject) => {
		try {
			MenuModule.updateMenu(values)
				.then(() => {
					Notifications.showNotif({
						header: 'Information!',
						message: 'Data has been updated successfully',
						type: Notifications.TYPE.SUCCESS,
					});
					resolve({ error: false, message: 'successfully' });
				})
				.catch((err) => {
					Notifications.showNotif({
						header: 'Warning!!',
						message: err,
						type: Notifications.TYPE.ERROR,
					});
					reject(new Error(err));
				});
		} catch (e) {
			reject(new Error(e.message));
		}
	});
	return newResponse;
};

const handleDelete = (values) => {
	const { username } = getRequester();
	const newValues = { _id: values._id, username };

	const newResponse = new Promise((resolve, reject) => {
		try {
			if (values._id) {
				Swal.fire({
					title: 'Are you sure?',
					text: 'Please check your entries !',
					icon: 'question',
					showCancelButton: true,
					confirmButtonText: 'Yes',
				}).then((result) => {
					if (result.isConfirmed) {
						MenuModule.deleteMenu(newValues)
							.then(() => {
								Notifications.showNotif({
									header: 'Information!',
									message: 'Data has been deleted successfully',
									type: Notifications.TYPE.SUCCESS,
								});
								resolve({ error: false, message: 'successfully' });
							})
							.catch((err) => {
								Notifications.showNotif({
									header: 'Warning!!',
									message: err,
									type: Notifications.TYPE.ERROR,
								});
								reject(new Error(err));
							});
					} else if (result.dismiss === Swal.DismissReason.cancel) {
						Swal.fire('Cancelled', 'Your data is safe :)', 'error');
						reject(new Error('Cancelled'));
					}
				});
			}
		} catch (e) {
			reject(new Error({ error: true }));
		}
	});
	return newResponse;
};

const FormCustom = ({
	initialValues,
	perPage,
	curPage,
	showAll,
	handleCustomSubmit,
	isUpdate,
	isReadOnly,
}) => {
	const { darkModeStatus } = useDarkMode();
	const { username } = getRequester();

	const validate = (values) => {
		const errors = {};

		if (!values.name) errors.name = 'Required';
		if (!values.icon) errors.icon = 'Required';
		if (!values.link) errors.link = 'Required';

		return errors;
	};

	const formik = useFormik({
		initialValues: { ...initialValues },
		validate,
		onSubmit: (values, { setSubmitting, resetForm, setErrors }) => {
			try {
				Swal.fire({
					title: 'Are you sure?',
					text: 'Please check your entries !',
					icon: 'question',
					showCancelButton: true,
					confirmButtonText: 'Yes',
				}).then((result) => {
					if (result.isConfirmed) {
						const newValues = { ...values, requester: username };
						handleCustomSubmit(
							{
								values: newValues,
								options: { curPage, perPage, showAll },
							},
							resetForm,
						);
					} else if (result.dismiss === Swal.DismissReason.cancel) {
						Swal.fire('Cancelled', 'Your data is safe :)', 'error');
					}
				});
			} catch (error) {
				setErrors({ submit: error.message });
				Swal.fire('Information ', 'Please check your entries again!', 'error');
			} finally {
				setSubmitting(false);
			}
		},
	});

	return (
		<Card
			shadow='none'
			borderSize={1}
			tag='form'
			noValidate
			onSubmit={formik.handleSubmit}
			className={isUpdate ? 'col-md-12 p-1' : 'col-md-4 p-1'}>
			<CardBody>
				<div className='row'>
					<div className='col-md-12'>
						<FormGroup id='name' label='Name' className='my-2'>
							<Input
								onChange={formik.handleChange}
								onBlur={formik.handleBlur}
								value={formik.values.name}
								isValid={formik.isValid}
								isTouched={formik.touched.name}
								invalidFeedback={formik.errors.name}
								autoComplete='off'
								disabled={isReadOnly}
							/>
						</FormGroup>
					</div>
					<div className='col-md-12'>
						<FormGroup id='icon' label='Icon' className='my-2'>
							<Input
								onChange={formik.handleChange}
								onBlur={formik.handleBlur}
								value={formik.values.icon}
								isValid={formik.isValid}
								isTouched={formik.touched.icon}
								invalidFeedback={formik.errors.icon}
								autoComplete='off'
								disabled={isReadOnly}
							/>
						</FormGroup>
					</div>
					<div className='col-md-12'>
						<FormGroup id='link' label='Link' className='my-2'>
							<Input
								onChange={formik.handleChange}
								onBlur={formik.handleBlur}
								value={formik.values.link}
								isValid={formik.isValid}
								isTouched={formik.touched.link}
								invalidFeedback={formik.errors.link}
								autoComplete='off'
								disabled={isReadOnly}
							/>
						</FormGroup>
					</div>
					{!isReadOnly && (
						<div className='col-md-12 my-2'>
							<Button
								icon='Save'
								color='success'
								type='submit'
								className='mx-1 float-end'
								isLight={darkModeStatus}
								onClick={formik.handleSubmit}>
								Submit
							</Button>
							{!isUpdate && (
								<Button
									icon='Clear'
									color='danger'
									type='reset'
									className='mx-1 float-end'
									isLight={darkModeStatus}
									onClick={formik.handleReset}>
									Clear
								</Button>
							)}
						</div>
					)}
				</div>
			</CardBody>
		</Card>
	);
};

FormCustom.propTypes = {
	initialValues: PropTypes.instanceOf(Array),
	perPage: PropTypes.number,
	curPage: PropTypes.number,
	showAll: PropTypes.bool,
	handleCustomSubmit: PropTypes.func,
	isUpdate: PropTypes.bool,
	isReadOnly: PropTypes.bool,
};
FormCustom.defaultProps = {
	initialValues: {
		icon: '',
		name: '',
		link: '',
	},
	perPage: 10,
	curPage: 1,
	showAll: false,
	handleCustomSubmit: () => {},
	isUpdate: false,
	isReadOnly: false,
};

const FormCustomModal = ({
	initialValues,
	handleCustomUpdate,
	handleCustomDelete,
	curPage,
	perPage,
	showAll,
}) => {
	const { darkModeStatus } = useDarkMode();
	const [isOpen, setOpen] = useState(false);
	const [isReadOnly, setReadOnly] = useState(false);

	const [title, setTitle] = useState('');

	return (
		<>
			<Button
				icon='GridOn'
				color='info'
				type='button'
				className='mx-1'
				isLight={darkModeStatus}
				onClick={() => {
					setTitle('Detail');
					setReadOnly(true);
					setOpen(true);
				}}
			/>

			<Button
				icon='Edit'
				color='success'
				type='button'
				className='mx-1'
				isLight={darkModeStatus}
				onClick={() => {
					setTitle('Update');
					setReadOnly(false);
					setOpen(true);
				}}
			/>

			<Button
				icon='Delete'
				color='danger'
				type='button'
				className='mx-1'
				isLight={darkModeStatus}
				onClick={() =>
					handleCustomDelete({
						values: initialValues,
						options: { curPage, perPage, showAll },
					})
				}
			/>

			<Modal isOpen={isOpen} setIsOpen={setOpen} titleId='modal-crud'>
				<ModalHeader setIsOpen={setOpen} className='p-4'>
					<ModalTitle id='modal-crud'>{title} Data</ModalTitle>
				</ModalHeader>
				<ModalBody>
					<FormCustom
						initialValues={initialValues}
						curPage={curPage}
						perPage={perPage}
						isReadOnly={isReadOnly}
						isUpdate={isOpen}
						showAll={showAll}
						handleCustomSubmit={handleCustomUpdate}
					/>
				</ModalBody>
			</Modal>
		</>
	);
};

FormCustomModal.propTypes = {
	initialValues: PropTypes.instanceOf(Object),
	handleCustomUpdate: PropTypes.func,
	handleCustomDelete: PropTypes.func,
	curPage: PropTypes.number,
	perPage: PropTypes.number,
	showAll: PropTypes.bool,
};
FormCustomModal.defaultProps = {
	handleCustomUpdate: null,
	handleCustomDelete: null,
	initialValues: {
		_id: null,
		icon: '',
		name: '',
		link: '',
	},
	curPage: 1,
	perPage: 10,
	showAll: false,
};

const TableCustom = ({
	data,
	totalRows,
	perPage,
	curPage,
	loading,
	showAll,
	fetchData,
	handleCustomUpdate,
	handleCustomDelete,
}) => {
	const { darkModeStatus } = useDarkMode();

	const handlePageChange = (page) => {
		fetchData(page, perPage, showAll);
	};

	const handlePerRowsChange = async (newPerPage, page) => {
		const all = newPerPage === totalRows;
		return fetchData(page, newPerPage, all);
	};

	const paginationComponentOptions = {
		selectAllRowsItem: true,
		selectAllRowsItemText: 'ALL',
	};

	const columns = useMemo(
		() => [
			{
				name: 'Name',
				selector: (row) => row.name,
				sortable: true,
				width: '300px',
			},
			{
				name: 'Icon',
				selector: (row) => row.icon,
				sortable: true,
				width: '300px',
			},
			{
				name: 'Link',
				selector: (row) => row.link,
				sortable: true,
				width: '500px',
			},
			{
				name: 'Action',
				width: '200px',
				// eslint-disable-next-line react/no-unstable-nested-components
				cell: (row) => {
					const initialValues = row;

					return (
						<FormCustomModal
							initialValues={initialValues}
							perPage={perPage}
							curPage={curPage}
							showAll={showAll}
							handleCustomUpdate={handleCustomUpdate}
							handleCustomDelete={handleCustomDelete}
						/>
					);
				},
			},
		],
		// eslint-disable-next-line react-hooks/exhaustive-deps
		[curPage, perPage, showAll],
	);
	return (
		<DarkDataTable
			columns={columns}
			data={data}
			progressPending={loading}
			pagination
			paginationServer
			paginationTotalRows={totalRows}
			onChangeRowsPerPage={handlePerRowsChange}
			onChangePage={handlePageChange}
			paginationComponentOptions={paginationComponentOptions}
			theme={darkModeStatus ? 'custom_dark' : 'light'}
		/>
	);
};

TableCustom.propTypes = {
	data: PropTypes.instanceOf(Array),
	totalRows: PropTypes.number,
	perPage: PropTypes.number,
	curPage: PropTypes.number,
	loading: PropTypes.bool,
	showAll: PropTypes.bool,
	fetchData: PropTypes.func,
	handleCustomUpdate: PropTypes.func,
	handleCustomDelete: PropTypes.func,
};
TableCustom.defaultProps = {
	data: [],
	totalRows: 0,
	perPage: 10,
	curPage: 1,
	loading: false,
	showAll: false,
	fetchData: () => {},
	handleCustomUpdate: () => {},
	handleCustomDelete: () => {},
};

const ExampleCRUD = () => {
	const { t } = useTranslation('crud');

	const [data, setData] = useState([]);
	const [totalRows, setTotalRows] = useState(0);
	const [perPage, setPerPage] = useState(10);
	const [curPage, setCurPage] = useState(1);

	const [showAll, setShowAll] = useState(false);
	const [loading, setLoading] = useState(false);

	const initialValues = {
		name: '',
		icon: '',
		link: '',
	};

	const createSubmit = (object, resetForm) => {
		const { values, options } = object;
		handleCreate(values)
			.then(() => {
				fetchData(options.curPage, options.perPage, options.showAll);
				resetForm(initialValues);
			})
			.catch(() => {})
			.finally(() => {});
	};

	const updateSubmit = (object, resetForm) => {
		const { values, options } = object;
		handleUpdate(values)
			.then(() => {
				fetchData(options.curPage, options.perPage, options.showAll);
				resetForm(initialValues);
			})
			.catch(() => {})
			.finally(() => {});
	};

	const deleteSubmit = (object) => {
		const { values, options } = object;
		handleDelete(values)
			.then(() => {
				fetchData(options.curPage, options.perPage, options.showAll);
			})
			.catch(() => {})
			.finally(() => {});
	};

	useEffect(() => {
		fetchData(curPage, perPage, showAll);
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const fetchData = async (newPage, newPerPage, all) => {
		setLoading(true);
		setShowAll(all);

		const query = `page=${newPage}&sizePerPage=${newPerPage}&show_all=${all}`;
		return MenuModule.readMenu(query)
			.then((response) => {
				setData(response.foundData);
				setTotalRows(response.countData);
				setCurPage(response.currentPage);
				setPerPage(newPerPage);
			})
			.catch(() => {})
			.finally(() => {
				setLoading(false);
			});
	};

	return (
		<PageWrapper title={t('Example CRUD')}>
			<PageLayoutHeader />
			<Page container='fluid'>
				<Card>
					<CardBody>
						<Accordion
							id='accordion-form'
							activeItemId='form'
							shadow='sm'
							color='info'
							stretch>
							<AccordionItem id='form-create' title={t('form')} icon='AddBox'>
								<FormCustom
									initialValues={initialValues}
									perPage={perPage}
									curPage={curPage}
									showAll={showAll}
									handleCustomSubmit={createSubmit}
								/>
							</AccordionItem>
						</Accordion>
					</CardBody>
				</Card>

				<Card stretch>
					<CardHeader borderSize={1}>
						<CardLabel icon='History' iconColor='info'>
							<CardTitle>{t('Historical Data')}</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody>
						<TableCustom
							data={data}
							totalRows={totalRows}
							perPage={perPage}
							curPage={curPage}
							loading={loading}
							fetchData={fetchData}
							setShowAll={setShowAll}
							handleCustomUpdate={updateSubmit}
							handleCustomDelete={deleteSubmit}
						/>
					</CardBody>
				</Card>
			</Page>
		</PageWrapper>
	);
};

ExampleCRUD.propTypes = {};
ExampleCRUD.defaultProps = {};

export default ExampleCRUD;

import React, { useState, useEffect, useMemo } from 'react';
import { Formik, Form } from 'formik';
import * as yup from 'yup';
import Swal from 'sweetalert2';
import { useTranslation } from 'react-i18next';
import PageWrapper from '../layout/PageWrapper/PageWrapper';
import Page from '../layout/Page/Page';
import PageLayoutHeader from '../pages/common/Headers/PageLayoutHeader';
import Card, { CardBody, CardHeader, CardLabel, CardTitle } from '../components/bootstrap/Card';
import FormGroup from '../components/bootstrap/forms/FormGroup';
import Input from '../components/bootstrap/forms/Input';
import Button from '../components/bootstrap/Button';
import MenuModule from '../modules/MenuModule';
import showNotification from '../components/extras/showNotification';
import DarkDataTable from '../components/DarkDataTable';
import useDarkMode from '../hooks/useDarkMode';
import Modal, {
	ModalBody,
	ModalHeader,
	ModalTitle,
	ModalFooter,
} from '../components/bootstrap/Modal';

const handleSubmit = (values) => {
	if (values.name && values.icon) {
		delete values.loading;
		return Swal.fire({
			title: 'Are you sure?',
			text: 'Please check your entries !',
			icon: 'info',
			showCancelButton: true,
			confirmButtonText: 'Yes',
		}).then((result) => {
			if (result.value) {
				MenuModule.create(values)
					.then((res) => {
						showNotification('Success!', res.status, 'success');
					})
					.catch((err) => {
						showNotification('Warning!', err, 'danger');
					});
			} else if (result.dismiss === Swal.DismissReason.cancel) {
				Swal.fire('Cancelled', 'Your data is safe :)', 'error');
			}
		});
	}
	return values;
};
const handleSubmitEdit = (values) => {
	if (values.name && values.icon) {
		return Swal.fire({
			title: 'Are you sure?',
			text: 'Please check your entries !',
			icon: 'info',
			showCancelButton: true,
			confirmButtonText: 'Yes',
		}).then((result) => {
			if (result.value) {
				MenuModule.edit(values)
					.then((res) => {
						showNotification('Success!', res.status, 'success');
					})
					.catch((err) => {
						showNotification('Warning!', err, 'danger');
					});
			} else if (result.dismiss === Swal.DismissReason.cancel) {
				Swal.fire('Cancelled', 'Your data is safe :)', 'error');
			}
		});
	}
	return values;
};
const FormExample = (dt) => {
	const { initialValues } = dt;
	const onSubmit = (values, { setSubmitting, resetForm, setStatus, setErrors }) => {
		try {
			handleSubmit(values);
			resetForm(initialValues);
			setStatus({ success: true });
		} catch (error) {
			setStatus({ success: false });
			setSubmitting(false);
			setErrors({ submit: error.message });
		}
	};
	const validationSchema = yup.object({
		name: yup.string().required('Field is required'),
		icon: yup.string().required('Field is required'),
		link: yup.string().required('Field is required'),
	});
	return (
		<Formik
			enableReinitialize
			initialValues={{ ...initialValues }}
			onSubmit={onSubmit}
			validationSchema={validationSchema}>
			{(formikField) => {
				return (
					<Form>
						<FormGroup id='name' label='Menu Name' className='col-md-12'>
							<Input
								onChange={formikField.handleChange}
								onBlur={formikField.handleBlur}
								value={formikField.values.name}
								isValid={formikField.isValid}
								isTouched={formikField.touched.name}
								invalidFeedback={formikField.errors.name}
								autoComplete='off'
							/>
						</FormGroup>
						<FormGroup id='icon' label='Icon' className='col-md-12'>
							<Input
								onChange={formikField.handleChange}
								onBlur={formikField.handleBlur}
								value={formikField.values.icon}
								isValid={formikField.isValid}
								isTouched={formikField.touched.icon}
								invalidFeedback={formikField.errors.icon}
								autoComplete='off'
							/>
						</FormGroup>
						<FormGroup id='link' label='Path' className='col-md-12'>
							<Input
								onChange={formikField.handleChange}
								onBlur={formikField.handleBlur}
								value={formikField.values.link}
								isValid={formikField.isValid}
								isTouched={formikField.touched.link}
								invalidFeedback={formikField.errors.link}
								autoComplete='off'
							/>
						</FormGroup>
						<br />
						<div className='col-md-12 '>
							<Button
								icon='Save'
								isOutline
								type='submit'
								color='success'
								className='float-end'
								isDisable={!formikField.isValid && !!formikField.submitCount}>
								Submit
							</Button>
						</div>
					</Form>
				);
			}}
		</Formik>
	);
};

// modal form using useFormik
const FormExampleEdit = (dt) => {
	const [isOpen, setIsOpen] = useState(false);

	const { initialValues } = dt;
	const onSubmit = (values, { setSubmitting, resetForm, setStatus, setErrors }) => {
		try {
			delete values.loading;
			handleSubmitEdit(values);
			resetForm(initialValues);
			setStatus({ success: true });
		} catch (error) {
			setStatus({ success: false });
			setSubmitting(false);
			setErrors({ submit: error.message });
		}
	};
	const validate = (values) => {
		const errors = {};

		if (!values.name) {
			errors.name = 'Field is required';
		}
		if (!values.icon) {
			errors.icon = 'Field is required';
		}
		if (!values.link) {
			errors.link = 'Field is required';
		}

		return errors;
	};
	return (
		<>
			<Button
				icon='Edit'
				isOutline
				type='button'
				color='primary'
				onClick={() => setIsOpen(true)}>
				Edit
			</Button>
			<Modal isOpen={isOpen} setIsOpen={setIsOpen} size='lg' titleId='modal-edit-menu-crud'>
				<ModalHeader setIsOpen={setIsOpen} className='p-4'>
					<ModalTitle id='modal-edit-menu-crud'>Update Data</ModalTitle>
				</ModalHeader>
				<Formik
					enableReinitialize
					initialValues={{ ...initialValues }}
					onSubmit={onSubmit}
					validate={validate}>
					{(formikField) => {
						return (
							<Form>
								<ModalBody className='px-4'>
									<FormGroup id='name' label='Menu Name' className='col-md-12'>
										<Input
											onChange={formikField.handleChange}
											onBlur={formikField.handleBlur}
											value={formikField.values.name}
											isValid={formikField.isValid}
											isTouched={formikField.touched.name}
											invalidFeedback={formikField.errors.name}
											autoComplete='off'
										/>
									</FormGroup>
									<FormGroup id='icon' label='Icon' className='col-md-12'>
										<Input
											onChange={formikField.handleChange}
											onBlur={formikField.handleBlur}
											value={formikField.values.icon}
											isValid={formikField.isValid}
											isTouched={formikField.touched.icon}
											invalidFeedback={formikField.errors.icon}
											autoComplete='off'
										/>
									</FormGroup>
									<FormGroup id='link' label='Path' className='col-md-12'>
										<Input
											onChange={formikField.handleChange}
											onBlur={formikField.handleBlur}
											value={formikField.values.link}
											isValid={formikField.isValid}
											isTouched={formikField.touched.link}
											invalidFeedback={formikField.errors.link}
											autoComplete='off'
										/>
									</FormGroup>
								</ModalBody>
								<ModalFooter className='px-4 pb-4'>
									<div className='col-md-12 '>
										<Button
											icon='Save'
											isOutline
											type='submit'
											color='success'
											className='float-end'
											isDisable={
												!formikField.isValid && !!formikField.submitCount
											}>
											Update
										</Button>
									</div>
								</ModalFooter>
							</Form>
						);
					}}
				</Formik>
			</Modal>
		</>
	);
};
const CustomButton = (dt) => {
	const { row } = dt;
	const { name, icon, _id, link } = row;
	const initialValues = {
		loading: false,
		name,
		icon,
		_id,
		link,
	};
	return <FormExampleEdit initialValues={initialValues} />;
};

const CustomDataTable = () => {
	const { darkModeStatus } = useDarkMode();
	const [data, setData] = useState([]);
	const [loading, setLoading] = useState(false);
	const [totalRows, setTotalRows] = useState(0);
	const [perPage, setPerPage] = useState(10);

	const fetchUsers = async (page) => {
		setLoading(true);
		return MenuModule.readMenu(`page=${page}&sizerPerPage=${perPage}`).then((res) => {
			setData(res.foundData);
			setTotalRows(res.countData);
			setLoading(false);
		});
	};

	const handlePageChange = (page) => {
		fetchUsers(page);
	};

	const handlePerRowsChange = async (newPerPage, page) => {
		setLoading(true);

		return MenuModule.readMenu(`page=${page}&sizerPerPage=${newPerPage}`).then((res) => {
			setData(res.foundData);
			setPerPage(newPerPage);
			setLoading(false);
		});
	};

	useEffect(() => {
		fetchUsers(1); // fetch page 1 of users

		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const columns = useMemo(
		() => [
			{
				name: 'Name',
				selector: (row) => row.name,
				sortable: true,
			},
			{
				name: 'Icon',
				selector: (row) => row.icon,
				sortable: true,
			},
			{
				name: 'Action',
				// eslint-disable-next-line react/no-unstable-nested-components
				cell: (dt) => {
					return <CustomButton row={dt} />;
				},
			},
		],
		[],
	);

	return (
		<DarkDataTable
			columns={columns}
			data={data}
			progressPending={loading}
			pagination
			paginationServer
			paginationTotalRows={totalRows}
			onChangeRowsPerPage={handlePerRowsChange}
			onChangePage={handlePageChange}
			theme={darkModeStatus ? 'custom_dark' : 'light'}
		/>
	);
};
const CRUD = () => {
	const initialValues = {
		loading: false,
		name: '',
		icon: '',
		link: '',
	};
	const { t } = useTranslation('crud');

	const [title] = useState({ title: 'CRUD' });
	return (
		<PageWrapper title={title.title}>
			<PageLayoutHeader />
			<Page container='fluid'>
				<Card className='col-md-4'>
					<CardHeader borderSize={1}>
						<CardLabel>
							<CardTitle>{t('form')}</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody>
						<div className='col-12 mb-3 g-4'>
							<FormExample initialValues={initialValues} />
						</div>
					</CardBody>
				</Card>
				<Card stretch>
					<CardHeader borderSize={1}>
						<CardLabel>
							<CardTitle>{t('historical_data')}</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody>
						<CustomDataTable />
					</CardBody>
				</Card>
			</Page>
		</PageWrapper>
	);
};

export default CRUD;

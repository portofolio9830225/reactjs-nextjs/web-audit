import React from 'react';
import { useMeasure } from 'react-use';

import Package from '../../../package.json';

const Footer = () => {
	const [ref, { height }] = useMeasure();

	const root = document.documentElement;
	root.style.setProperty('--footer-height', `${height}px`);

	return (
		<footer ref={ref} className='footer'>
			<div className='container-fluid'>
				<div className='row'>
					<div className='col'>
						<span className='fw-light'>
							Copyright © {Package.release_year} - Version {Package.version}
						</span>
					</div>
				</div>
			</div>
		</footer>
	);
};

export default Footer;

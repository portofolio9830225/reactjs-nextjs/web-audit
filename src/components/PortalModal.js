import React from 'react';
import ReactDOM from 'react-dom';
import { ModalContext } from './modalContext'; // eslint-disable-line import/no-cycle
import Modal, { ModalBody, ModalHeader, ModalTitle } from './bootstrap/Modal';

const PortalModal = () => {
	const { modalContent, handleModal, modal } = React.useContext(ModalContext);
	if (modal) {
		return ReactDOM.createPortal(
			<Modal setIsOpen={() => handleModal()} isOpen={modal} size='lg' isScrollable>
				<ModalHeader className='px-4' setIsOpen={() => handleModal()}>
					<ModalTitle id='project-edit'>New Card</ModalTitle>
				</ModalHeader>
				<ModalBody className='px-4'>{modalContent}</ModalBody>
			</Modal>,
			document.querySelector('#modal-root'),
		);
	}
	return null;
};

export default PortalModal;

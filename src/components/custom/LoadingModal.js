import React from 'react';
import PropTypes from 'prop-types';
import Modal, { ModalBody } from '../bootstrap/Modal';

const LoadingModal = ({ loading, setLoading }) => {
	return (
		<Modal isOpen={loading} size='sm' isCentered setIsOpen={setLoading} isStaticBackdrop>
			<ModalBody
				style={{ backgroundColor: '#6c5dd3', color: 'white' }}
				className='text-center'>
				<button className='btn btn-primary' type='button' disabled>
					<span
						className='spinner-grow spinner-grow-sm'
						role='status'
						aria-hidden='true'
					/>
					&nbsp;
					<span
						className='spinner-grow spinner-grow-sm'
						role='status'
						aria-hidden='true'
					/>
					&nbsp;
					<span
						className='spinner-grow spinner-grow-sm'
						role='status'
						aria-hidden='true'
					/>
					&nbsp;
					<span className='sr-only'>Loading...</span>
				</button>
			</ModalBody>
		</Modal>
	);
};

LoadingModal.propTypes = {
	loading: PropTypes.bool,
	setLoading: PropTypes.func,
};
LoadingModal.defaultProps = {
	loading: false,
	setLoading: () => {},
};

export default LoadingModal;
